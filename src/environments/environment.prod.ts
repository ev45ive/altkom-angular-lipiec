export const environment = {
  production: true,
  music: {
    search_url: 'https://api.spotify.com/v1/search',
    auth_options: {
      auth_url: 'https://accounts.spotify.com/authorize',
      client_id: '512050833956472d9ca6b740c94f15f6',
      response_type: 'token',
      redirect_uri: 'http://localhost:4200/'
    }
  }
};
