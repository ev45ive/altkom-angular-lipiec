import { Component, OnInit } from '@angular/core';
import { Playlist } from 'src/app/model/playlist';
import { PlaylistsService } from '../playlists.service';
import { Observable } from 'rxjs';
import { Router, ActivatedRoute } from '../../../../node_modules/@angular/router';
import { map, switchMap } from '../../../../node_modules/rxjs/operators';


@Component({
  selector: 'app-playlists',
  template: `
    <!-- .row>.col*2 -->

    <div class="row">
      <div class="col">
        <app-items-list 
            [items]="playlists$ | async" 
            [selected]="selected$ | async"
            (selectedChange)="select($event)">
        </app-items-list>
      </div>
      <div class="col">
          <router-outlet></router-outlet>

      </div>
    </div>
  `,
  styles: []
})
export class PlaylistsComponent implements OnInit {

  selected$: Observable<Playlist>

  playlists$ = this.playlistsService.getPlaylists()

  constructor(
    private playlistsService: PlaylistsService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  select(playlist: Playlist) {
    if (playlist) {
      this.router.navigate(['/playlists', playlist.id])
    } else {
      this.router.navigate(['/playlists'])
    }
  }

  ngOnInit() {
    this.selected$ = this.route.paramMap.pipe(
      map(paramMap => parseInt(paramMap.get('id'))),
      switchMap(id => this.playlistsService.getPlaylist(id))
    )
  }

}
