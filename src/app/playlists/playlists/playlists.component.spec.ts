import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlaylistsComponent } from './playlists.component';
import { CUSTOM_ELEMENTS_SCHEMA, Component, EventEmitter } from '@angular/core';
import { By } from '@angular/platform-browser';

@Component({
  selector: 'app-items-list',
  template: '',
  inputs: [
    'items:items',
    'selected:selected'
  ],
  outputs:[
    'selectedChange'
  ]
})
export class ItemsListComponent {
  selectedChange = new EventEmitter()
 }

@Component({
  selector: 'app-playlist-details',
  template: '',
  inputs: [
    'playlist:playlist',
  ]
})
export class PlaylistDetailsComponent { }

describe('PlaylistsComponent', () => {
  let component: PlaylistsComponent;
  let fixture: ComponentFixture<PlaylistsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PlaylistsComponent, ItemsListComponent, PlaylistDetailsComponent],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlaylistsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should provide playlist to items list', () => {
    component.playlists = [{ id: 12, name: "Test", favourite: true, color: '#ff0000' }]
    fixture.detectChanges();

    const itemsList = fixture.debugElement.query(By.css('app-items-list'))
    expect(itemsList.componentInstance.items).toEqual(component.playlists)
  });

  it('should listen on items list for selection changes',()=>{
    const playlist = { id: 12, name: "Test", favourite: true, color: '#ff0000' };

    const itemsList = fixture.debugElement.query(By.css('app-items-list'))
    ;(itemsList.componentInstance as ItemsListComponent).selectedChange.emit(playlist)

    expect(component.selected).toEqual(playlist)
  })
});
