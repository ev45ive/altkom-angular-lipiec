import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PlaylistsRoutingModule } from './playlists-routing.module';
import { PlaylistsComponent } from './playlists/playlists.component';
import { ItemsListComponent } from './items-list/items-list.component';
import { ListItemComponent } from './list-item/list-item.component';
import { PlaylistDetailsComponent } from './playlist-details/playlist-details.component';
import { FormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';
import { PlaylistContainerComponent } from './playlist-container/playlist-container.component'

@NgModule({
  imports: [
    CommonModule,
    PlaylistsRoutingModule,
    FormsModule,
    SharedModule
  ],
  declarations: [
    PlaylistsComponent,
    ItemsListComponent,
    ListItemComponent,
    PlaylistDetailsComponent,
    PlaylistContainerComponent
  ],
  exports: [
    // PlaylistsComponent
  ]
})
export class PlaylistsModule { }
