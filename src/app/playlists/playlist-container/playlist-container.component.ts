import { Component, OnInit } from '@angular/core';
import { PlaylistsService } from '../playlists.service';
import { ActivatedRoute } from '../../../../node_modules/@angular/router';
import { map, switchMap } from '../../../../node_modules/rxjs/operators';

@Component({
  selector: 'app-playlist-container',
  template: `
    <app-playlist-details *ngIf="playlist$ | async as playlist" 
      (save)="save($event)" 
      [playlist]="playlist">
    </app-playlist-details> 
  `,
  styles: []
})
export class PlaylistContainerComponent implements OnInit {

  playlist$

  save(playlist) {
    this.service.save(playlist)
  }

  constructor(
    private service: PlaylistsService,
    private route: ActivatedRoute
  ) {
    this.playlist$ = this.route.paramMap.pipe(
      map(paramMap => parseInt(paramMap.get('id'))),
      switchMap(id => this.service.getPlaylist(id))
    )
  }

  ngOnInit() {
  }

}
