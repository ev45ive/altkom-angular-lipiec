import { Injectable } from '@angular/core';
import { Playlist } from '../model/playlist';
import { of, BehaviorSubject } from 'rxjs';
import { map } from '../../../node_modules/rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PlaylistsService {

  playlists = new BehaviorSubject<Playlist[]>([
    {
      id: 123, name: 'Angular greatest Hits', favourite: false, color: "#ff0000"
    },
    {
      id: 234, name: 'The best of Angular ', favourite: true, color: "#00ff00"
    },
    {
      id: 345, name: 'Angular Top 20', favourite: false, color: "#0000ff"
    },
  ])

  constructor() { }

  getPlaylists() {
    return this.playlists.asObservable()
  }

  getPlaylist(id: number) {
    return this.getPlaylists().pipe(
      map(playlists => {
        return playlists.find(p => p.id == id)
      })
    )

  }

  save(playlist: Playlist) {
    this.playlists.next(this.playlists.getValue().map(p => {
      if (p.id == playlist.id) {
        return playlist
      } else {
        return p
      }
    }))
  }
}

