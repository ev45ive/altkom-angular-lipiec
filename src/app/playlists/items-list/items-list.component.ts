import { Component, OnInit, ViewEncapsulation, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';
import { Playlist } from '../../model/playlist';


// [style.border-left-color]="hover==playlist? playlist.color : 'unset'"
// (mouseenter)="hover = playlist"
// (mouseleave)="hover = null"

@Component({
  selector: 'app-items-list',
  template: `
    <div class="list-group">
        <div class="list-group-item list-group-item-action" 

          (click)="select(playlist)"
          [class.active]="playlist.id == selected?.id"

          [appHighlight]="playlist.color"

         *ngFor="let playlist of playlists; let i = index; trackBy trackFn">
          {{i+1}}. {{playlist.name}}
        </div>
    </div>
    {{selected?.name}}
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
  styles: [`
    .list-group-item{
      border-left:5px solid black;
    }
    .list-group-item-action{
      cursor:pointer;
    }
    :host-context(.themed) p{
      color: hotpink;
    }

    :host(.bordered){
      display:block;
      border:1px solid black;
    }
  `,
    // extraStyle
  ],
  /* inputs: [
    'playlists:items'
  ] */
})
export class ItemsListComponent implements OnInit {
  hover

  @Input('items') playlists: Playlist[]

  @Input()
  selected: Playlist

  @Output()
  selectedChange = new EventEmitter<Playlist>()

  select(playlist: Playlist) {
    const selected = this.selected === playlist ? null : playlist
    this.selectedChange.emit(selected)
  }

  constructor() { }

  ngOnInit() {
  }

  trackFn(index, item) {
    return index.id
  }

}
