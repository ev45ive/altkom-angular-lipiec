import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Playlist } from '../../model/playlist';

enum Modes {
  show, edit
}

@Component({
  selector: 'app-playlist-details',
  template: `
  <ng-container [ngSwitch]="mode" *ngIf="playlist">
    <div *ngSwitchDefault>
      <dl>
        <dt>Name:</dt>
        <dd>{{ playlist.name }}</dd>
        
        <dt>Favourite</dt>
        <dd [ngStyle]="{
          'fontSize.px': 20,
          'color':'darkgray' 
        }">{{playlist.favourite? 'Yes':'No'}}</dd>

        <dt>Color</dt>
        <dd [style.background-color]="playlist.color" [style.fontSize.px]="20">{{playlist.color}}</dd>
      </dl>
      <input type="button" value="Edit" class="btn btn-info" (click)="edit()">
    </div>
    
    <form *ngSwitchCase=" 'edit' " #formRef="ngForm" (ngSubmit)="save(formRef)">
      <div class="form-group">
        <label>Name: </label>
        <input type="text" class="form-control" [(ngModel)]="playlist.name" name="name" #modelRef="ngModel">
      </div>
      <div class="form-group">
        <label>Favourite: </label>
        <input type="checkbox" [ngModel]="playlist.favourite" name="favourite">
      </div>
      <div class="form-group">
        <label>Color: </label>
        <input type="color" [ngModel]="playlist.color" name="color">
      </div>
      <input type="button" value="Cancel" class="btn btn-danger" (click)="cancel()">
      <input type="submit" value="Save" class="btn btn-success" >
    </form>
    </ng-container>
  `,
  styles: []
})
export class PlaylistDetailsComponent implements OnInit {

  @Output('save')
  saveEvent = new EventEmitter()

  save(formRef) {
    const playlist = {
      ...this.playlist, ...formRef.value
    }
    this.saveEvent.emit(playlist)
  }

  @Input()
  playlist: Playlist

  mode = "show"

  edit() {
    this.mode = "edit"
  }
  cancel() {
    this.mode = "show"
  }


  constructor() { }

  ngOnInit() {
  }

}
