import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlaylistDetailsComponent } from './playlist-details.component';
import { FormsModule, NgModel } from '../../../../node_modules/@angular/forms';
import { Component } from '../../../../node_modules/@angular/core';
import { Playlist } from 'src/app/model/playlist';
import { By } from '../../../../node_modules/@angular/platform-browser';

@Component({
  template: `<app-playlist-details [playlist]="playlist"></app-playlist-details>`
})
export class TestComponent {
  playlist: Playlist = {
    id: 123, name: "Testing", color: "#ff0000", favourite: true
  }
}

describe('PlaylistDetailsComponent', () => {
  let component: TestComponent;
  let fixture: ComponentFixture<TestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [FormsModule],
      declarations: [TestComponent, PlaylistDetailsComponent],
      providers: []
    })
      .compileComponents()
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render playlist details', () => {

    expect(fixture.nativeElement.innerText).toMatch('Testing')
    expect(fixture.nativeElement.innerText).toMatch('Yes')
    expect(fixture.nativeElement.innerText).toMatch('#ff0000')
  })

  it('should render updated playlist details', () => {
    component.playlist.name = "Changed"
    component.playlist.favourite = false
    component.playlist.color = "#0000ff"

    fixture.detectChanges()

    expect(fixture.nativeElement.innerText).toMatch('Changed')
    expect(fixture.nativeElement.innerText).toMatch('No')
    expect(fixture.nativeElement.innerText).toMatch('#0000ff')
  })

  it('should views when mode changes', () => {
    const detailsComponent = fixture.debugElement.query(By.directive(PlaylistDetailsComponent));
    (detailsComponent.componentInstance as PlaylistDetailsComponent).mode = 'edit'

    fixture.detectChanges()
    const buttonSave = fixture.debugElement.query(By.css('[value=Save]'));

    expect(buttonSave).toBeTruthy()
  })

  it('should switch to edit mode', () => {
    const detailsComponent = fixture.debugElement.query(By.directive(PlaylistDetailsComponent));
    const buttonSave = fixture.debugElement.query(By.css('[value=Edit]'));

    buttonSave.triggerEventHandler('click',{})

    expect((detailsComponent.componentInstance as PlaylistDetailsComponent).mode).toEqual('edit')
  })

  it('should show playlist details in edit form',()=>{
    const detailsComponent = fixture.debugElement.query(By.directive(PlaylistDetailsComponent));
    (detailsComponent.componentInstance as PlaylistDetailsComponent).mode = 'edit'
    fixture.detectChanges()
    const field = fixture.debugElement.query(By.directive(NgModel));

    fixture.whenStable().then(()=>{
      expect(field.nativeElement.value).toEqual('Testing')
    })
  })
});
