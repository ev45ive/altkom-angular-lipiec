import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HighlightDirective } from './highlight.directive';
import { ShortenPipe } from './shorten.pipe';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [HighlightDirective, ShortenPipe],
  exports: [HighlightDirective, ShortenPipe]
})
export class SharedModule { }
