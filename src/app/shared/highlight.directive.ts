import { Directive, ElementRef, Input, OnInit, Attribute, HostBinding, HostListener, OnChanges } from '@angular/core';

@Directive({
  selector: '[appHighlight]',
  exportAs:'appHighlight'
  /*   host: {
      '[style.color]': 'appHighlight',
      '(mouseenter)': 'hover = $event'
    } */
})
export class HighlightDirective implements OnInit {

  @Input()
  appHighlight = 'hotpink'

  @HostBinding('style.border-left-color')
  get color() {
    return this.hover ? this.appHighlight : 'black'
  }

  hover = false

  @HostListener('mouseenter', ['$event.x', '$event.y'])
  onEnter() {
    this.hover = true
  }

  @HostListener('mouseleave', ['$event.x', '$event.y'])
  onLeave() {
    this.hover = false
  }

  constructor(
    public elem: ElementRef,
    @Attribute('color') color
  ) { }

  ngOnInit() {
  }

}