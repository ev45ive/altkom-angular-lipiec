import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MusicSearchComponent } from './music-search/music-search.component';
import { SearchFormComponent } from './search-form/search-form.component';
import { AlbumsGridComponent } from './albums-grid/albums-grid.component';
import { AlbumItemComponent } from './album-item/album-item.component';
import { environment } from 'src/environments/environment';
import { MusicService, SEARCH_URL } from './music.service';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '../../../node_modules/@angular/forms';
import { SharedModule } from '../shared/shared.module';
import { MusicRoutingModule } from './/music-routing.module';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    ReactiveFormsModule,
    SharedModule,
    MusicRoutingModule
  ],
  declarations: [
    MusicSearchComponent,
    SearchFormComponent,
    AlbumsGridComponent,
    AlbumItemComponent
  ],
  exports: [
    // MusicSearchComponent
  ],
  providers: [
    {
      provide: SEARCH_URL,
      useValue: environment.music.search_url
    },
    // {
    //   provide: 'MUSIC_SERVICE',
    //   useFactory: (search_url) => {
    //     return new MusicService(search_url)
    //   },
    //   deps: ['SEARCH_URL']
    // },
    // {
    //   provide: MusicService,
    //   useClass: SpecialChristmassPromoMusicService,
    // },
    // {
    //   provide:MusicService,
    //   useClass:MusicService
    // },
    // MusicService
  ]
})
export class MusicModule { }
