import { Component, OnInit, Input } from '@angular/core';
import { Album, AlbumImage } from '../../model/album.interface';

@Component({
  selector: 'app-album-item',
  template: `
    <img class="card-img-top" [src]="image.url">

    <div class="card-body">
      <h5 class="card-title">
        {{album.name | shorten: 20}}
      </h5>
    </div>
  `,
  styles: []
})
export class AlbumItemComponent implements OnInit {

  @Input('album')
  set getAlbum(album){
    this.album = album
    this.image = album.images[0]
  }

  album: Album

  image:AlbumImage

  constructor() { }

  ngOnInit() {
  }

}
