import { Component, OnInit, Inject } from '@angular/core';
import { Album } from '../../model/album.interface';
import { MusicService } from '../music.service';
import { Subscription, Subject, Observable } from 'rxjs';
import { takeUntil, map, catchError } from 'rxjs/operators';
import { Router, ActivatedRoute } from '../../../../node_modules/@angular/router';

@Component({
  selector: 'app-music-search',
  template: `
    <div class="row">
      <div class="col">
        <app-search-form 
            [query]="query$ | async"
            (queryChange)="search($event)">
        </app-search-form>
      </div>
    </div>
    <div class="row">
      <div class="col">
        <ng-container *ngIf="albums$ | async as albums">

          <p>Albums found: {{ albums.length }}</p>
          <app-albums-grid [albums]="albums"></app-albums-grid>

        </ng-container>
      </div>
    </div>
  `,
  styles: []
})
export class MusicSearchComponent implements OnInit {

  albums$ = this.musicSearch.getAlbums()
  query$ = this.musicSearch.getQuery()

  constructor(
    private musicSearch: MusicService,
    private router: Router,
    private route: ActivatedRoute
  ) {

    const query = this.route.snapshot.queryParamMap.get('query')
    this.search(query)

    this.query$.subscribe(query => {
      this.router.navigate(['/music'], {
        queryParams: {
          query
        }
      })
    })
  }

  search(query) {
    this.musicSearch.search(query);
  }

  ngOnInit() {
  }
}