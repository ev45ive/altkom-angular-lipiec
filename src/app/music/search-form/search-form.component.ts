import { Component, OnInit, Output, EventEmitter, Input, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators, ValidatorFn, AsyncValidatorFn, ValidationErrors, AbstractControl, FormControl } from '@angular/forms';
import { Observable, Observer } from 'rxjs';
import { map, filter, mapTo, combineLatest, withLatestFrom, merge } from '../../../../node_modules/rxjs/operators';

@Component({
  selector: 'app-search-form',
  template: `
    <!-- https://getbootstrap.com/docs/4.1/components/input-group/#button-addons -->

    <div class="form-group mb-3" [formGroup]="queryForm">
      <input type="search" class="form-control" placeholder="Search" formControlName="query">
      <p *ngIf="queryForm.pending">Please wait... checking...</p>
      <!-- <form-messages field="queryForm.get('query')"></form-messages> -->
      
      <ng-container *ngIf="queryForm.get('query') as field">
        <div class="errors" *ngIf="field.touched || field.dirty">      
          <div *ngIf="field.hasError('required')">Field is required</div>
          <div *ngIf="field.getError('minlength') as error">
          Value is too short. Only {{error.requiredLength - error.actualLength}} more to go!
          </div>
          <div *ngIf="field.getError('censor') as error">
            Queries including "{{error.badword}}" are not allowed!
          </div>
        </div>
      </ng-container>
    </div>
  `,
  // changeDetection: ChangeDetectionStrategy.OnPush,
  styles: [`
    .form-group .ng-invalid.ng-touched,
    .form-group .ng-invalid.ng-dirty{
        border: 2px solid red;
    }
    .errors{
      color:red;
    }
  `]
})
export class SearchFormComponent implements OnInit {

  queryForm: FormGroup

  @Output()
  queryChange = new EventEmitter<string>()

  @Input()
  set query(query) {
    (this.queryForm.get('query') as FormControl)
      .setValue(query, {
        // emitModelToViewChange: false
      })
  }

  constructor(private fb: FormBuilder, private cdr: ChangeDetectorRef) {

    const censor = (badword: string): ValidatorFn => (c: AbstractControl): ValidationErrors | null => {
      const hasError = (c.value as string).includes(badword)

      return hasError ? {
        'censor': { badword }
      } : null
    }

    const asyncCensor = (badword: string): AsyncValidatorFn => (c: AbstractControl): Observable<ValidationErrors | null> => {
      // return this.http.get('...').pipe(map(response => response.error)

      return Observable.create((observer: Observer<ValidationErrors | null>) => {

        // create observable source:
        const handler = setTimeout(() => {
          const hasError = (c.value as string).includes(badword)

          observer.next(hasError ? {
            'censor': { badword }
          } : null);
          observer.complete()

        }, 1000)

        // unsubscribe cleanup:
        return () => {
          clearTimeout(handler)
        }
      })
    }

    this.queryForm = this.fb.group({
      query: this.fb.control('', [
        Validators.required,
        Validators.minLength(3),
      ], [
          asyncCensor('batman')
        ])
    })

    const value$ = this.queryForm
      .get('query')
      .valueChanges

    const status$ = this.queryForm
      .get('query')
      .statusChanges

    // status$.pipe(merge(value$)).subscribe((x) => {
    //   this.cdr.detectChanges()
    // })

    const valid$ = status$.pipe(
      filter(status => status === "VALID"),
      mapTo(true)
    )

    valid$.pipe(
      withLatestFrom(value$, (valid, value) => value)
    )
      .subscribe(query => this.search(query));
  }


  search(query) {
    this.queryChange.emit(query)
  }


  ngOnInit() {
  }

  ngAfterViewInit() {
    // setTimeout(() => {
    //     this.cdr.detectChanges()
    // })
  }
}
