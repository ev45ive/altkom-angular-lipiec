import { MusicRoutingModule } from './music-routing.module';

describe('MusicRoutingModule', () => {
  let musicRoutingModule: MusicRoutingModule;

  beforeEach(() => {
    musicRoutingModule = new MusicRoutingModule();
  });

  it('should create an instance', () => {
    expect(musicRoutingModule).toBeTruthy();
  });
});
