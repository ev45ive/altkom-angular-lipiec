import { Injectable, Inject, InjectionToken, EventEmitter } from '@angular/core';
import { Album } from '../model/album.interface';
import { HttpClient } from '@angular/common/http'
import { SecurityService } from '../security/security.service';

import { map, catchError, startWith, debounceTime, filter, distinctUntilChanged, switchMap } from 'rxjs/operators'
import { throwError, of, Subject, BehaviorSubject } from 'rxjs';

export const SEARCH_URL = new InjectionToken<string>('Url for albums search')

interface AlbumsResponse {
  albums: {
    items: Album[]
  }
}

@Injectable({
  providedIn: 'root',
})
export class MusicService {

  albums$ = new BehaviorSubject<Album[]>([])
  query$ = new BehaviorSubject<string>('alice')

  constructor(
    @Inject(SEARCH_URL) private search_url: string,
    private http: HttpClient,
  ) {
    this.query$.pipe(
      debounceTime(400),
      filter(query => query && query.length >= 3),
      distinctUntilChanged(),
      map(query => ({ type: 'album', q: query })),
      switchMap(params => this.makeRequest(params)),
      map(response => response.albums.items)
    )
      .subscribe(albums => {
        this.albums$.next(albums)
      })
  }

  makeRequest(params) {
    return this.http.get<AlbumsResponse>(this.search_url, {
      params
    })
  }

  search(query) {
    this.query$.next(query)
  }

  getQuery() {
    return this.query$.asObservable()
  }

  getAlbums() {
    return this.albums$.asObservable()
  }

}

