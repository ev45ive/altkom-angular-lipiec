import { Component, OnInit, Input, ChangeDetectionStrategy } from '@angular/core';
import { Album } from '../../model/album.interface';

@Component({
  selector: 'app-albums-grid',
  template: `
  {{ticker}}
    <div class="card-group">
      <app-album-item class="card"
          [album]="album"
          *ngFor="let album of albums">
      </app-album-item>
    </div>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
  styles: [`
    .card{    
      flex:0 0 25%;
    }
  `]
})
export class AlbumsGridComponent implements OnInit {

  get ticker(){
    return Date.now()
  }

  @Input()
  albums:Album[]

  constructor() { }

  ngOnInit() {
  }

}
