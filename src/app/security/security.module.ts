import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AUTH_OPTIONS, SecurityService } from './security.service';
import { environment } from 'src/environments/environment';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthInterceptor } from './auth-interceptor';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [],
  providers: [
    {
      provide: AUTH_OPTIONS,
      useValue: environment.music.auth_options
    },
    AuthInterceptor,
    {
      provide: HTTP_INTERCEPTORS,
      useExisting: AuthInterceptor,
      multi: true
    },
  ]
})
export class SecurityModule {
  constructor(private security: SecurityService) {
    this.security.getToken()
  }
}
