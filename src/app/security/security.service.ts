import { Injectable, Inject, InjectionToken } from '@angular/core';
import { HttpParams } from '@angular/common/http';
import { DOCUMENT } from '@angular/common';

interface AuthOptions {
  auth_url: string;
  client_id: string;
  response_type: string;
  redirect_uri: string;
}

export const AUTH_OPTIONS = new InjectionToken('Auth Options')

@Injectable({
  providedIn: 'root'
})
export class SecurityService {

  constructor(
    @Inject(AUTH_OPTIONS) private auth_options: AuthOptions,
    @Inject(DOCUMENT) document: Document
  ) {
    this.location = document.location
  }
  storage: Storage = window.localStorage
  location: Location

  authorize() {
    const {
      auth_url, response_type, redirect_uri, client_id
    } = this.auth_options

    const p = new HttpParams({
      fromObject: {
        client_id,
        redirect_uri,
        response_type
      }
    })
    this.location.replace(auth_url + '?' + p.toString())
    this.storage.removeItem('token')
  }

  token: string

  getToken() {
    this.token = JSON.parse(this.storage.getItem('token'))

    if (!this.token && this.location.hash) {
      const p = new HttpParams({
        fromString: this.location.hash.substr(1)
      })
      this.token = p.get('access_token')

      this.storage.setItem('token', JSON.stringify(this.token))
      this.location.hash = ''
    }


    if (!this.token) {
      return this.authorize()
    }

    return this.token
  }
}
