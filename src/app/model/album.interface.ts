export interface Album {
    id: string;
    name: string;
    artists?: Artist[];
    images: AlbumImage[];
}


export interface Artist {
    id: string;
    name: string;
}

export interface AlbumImage {
    url: string;
}